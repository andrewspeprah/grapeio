require 'faker'

namespace :db do
  desc "Fill database with sample data"
  task :fill_privilege => :environment do
    Apartment::Tenant.switch!('grapeio')
  end
end