require 'faker'

namespace :db do
  desc "Fill database with sample data"
  task :populate => :environment do
    Apartment::Tenant.switch!('grapeio')
    100.times do |n|
      offer_type = Property::OFFER_TYPE.sample
      category = Property::CATEGORY.sample
      name = Property::PROPERTY_NAME.sample
      code = "#{name}-TS-#{Property.with_deleted.where(name: name).count + 1}"
      build_type = Property::BUILDING_TYPE.sample
      price = rand(10000)
      currency = Property::CURRENCIES.sample
      tenure = Property::TENURE.sample
      location = Faker::Address.full_address
      location_latlng = "#{Faker::Address.latitude},#{Faker::Address.longitude}"
      meter = Property::METER.sample
      no_of_tenant = rand(20)
      other_info = "Lorem other information"
      user_id = User.first.id
      pp "#{n}: Record for #{user_id}"
      owner = Keeper.create(name: Faker::Name.name, telephone: Faker::PhoneNumber.phone_number, location: Faker::Address.full_address, is_owner: true)
      bedrooms = rand(20)
      bathrooms = rand(20)
      size = rand(5000)
      size_unit = Property::SIZE_UNIT.sample
      features = Property::FEATURES.sample
      utilities = Property::UTILITIES.sample(2)
      landmark = Faker::Address.community
      water_source = Property::WATER_SOURCE.sample(2)
      location_city = Faker::Address.city
      location_state = Faker::Address.state
      location_postcode = Faker::Address.postcode
      location_road = Faker::Address.street_name
      location_suburb = Faker::Address.community
      has_caretaker = false
      property = Property.new(offer_type: offer_type,
                      category: category,
                      name: name,
                      code: code,
                      build_type: build_type,
                      price: price,
                      currency: currency,
                      tenure: tenure,
                      location: location,
                      location_latlng: location_latlng,
                      meter: meter,
                      no_of_tenant: no_of_tenant,
                      other_info: other_info,
                      user_id: user_id,
                      owner: owner,
                      bedrooms: bedrooms,
                      bathrooms: bathrooms,
                      size: size,
                      size_unit: size_unit,
                      features: features,
                      utilities: utilities,
                      landmark: landmark,
                      water_source: water_source,
                      location_city: location_city,
                      location_state: location_state,
                      location_postcode: location_postcode,
                      location_road: location_road,
                      location_suburb: location_suburb,
                      has_caretaker: has_caretaker)
      if property.save
      else
        pp property.errors.as_json
      end
    end
  end
end