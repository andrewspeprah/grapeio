require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  context "#index" do
    it "should be successful" do
      get :index
      expect(response).to render_template("index")
    end
  end
end
