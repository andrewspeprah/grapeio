class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.decimal :amount
      t.string  :currency
      t.string  :category
      t.string  :state
      t.json    :meta
      t.timestamps
    end
  end
end
