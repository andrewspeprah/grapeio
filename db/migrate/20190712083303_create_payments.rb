class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.decimal :amount
      t.string :currency
      t.integer :customer_id
      t.integer :seller_id
      t.datetime :date_sold
      t.string :transaction_type
      t.datetime :rent_from
      t.datetime :rent_to
      t.text :other_details
      t.integer :property_id
      t.timestamps
    end
  end
end
