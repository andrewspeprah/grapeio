class AddCityToProperty < ActiveRecord::Migration[5.2]
  def change
    add_column :properties, :location_city, :string
    add_column :properties, :location_state, :string
    add_column :properties, :location_postcode, :string
    add_column :properties, :location_road, :string
    add_column :properties, :location_suburb, :string
  end
end
