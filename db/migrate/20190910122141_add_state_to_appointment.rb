class AddStateToAppointment < ActiveRecord::Migration[5.2]
  def change
    add_column :appointments, :state, :string, default: "Date Fixed"
    add_column :comments, :state, :string, default: "Response pending"
    remove_column :call_logs, :state, :string
  end
end
