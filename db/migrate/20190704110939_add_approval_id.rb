class AddApprovalId < ActiveRecord::Migration[5.2]
  def change
    add_column :properties, :approved_by, :string
    add_column :properties, :approved_at, :datetime
  end
end
