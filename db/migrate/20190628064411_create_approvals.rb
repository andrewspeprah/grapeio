class CreateApprovals < ActiveRecord::Migration[5.2]
  def change
    create_table :approvals do |t|
      t.string  :state
      t.integer :approval_id
      t.string  :approval_type
      t.timestamps
    end
  end
end
