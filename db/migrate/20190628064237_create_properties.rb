class CreateProperties < ActiveRecord::Migration[5.2]
  def change
    create_table :properties do |t|
      t.string :offer_type, default: "rent"
      t.string :category, default: "CAT.A"
      t.string :code, default: ""
      t.string :build_type, default: "apartment"
      t.string :name, default: "SR"
      t.decimal :price
      t.string :currency
      t.string :tenure
      t.string :location
      t.string :location_latlng
      t.string :water_source
      t.string :meter
      t.string :no_of_tenant
      t.text   :other_info
      t.references :user
      t.references :owner
      t.timestamps
    end
  end
end
