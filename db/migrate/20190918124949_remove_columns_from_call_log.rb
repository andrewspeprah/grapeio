class RemoveColumnsFromCallLog < ActiveRecord::Migration[5.2]
  def change
    add_column :call_logs, :property_locations, :string, array: true, default: []
    remove_column :call_logs, :property_id, :integer
    remove_column :call_logs, :property_code, :string
    remove_column :call_logs, :property_suburb, :string
  end
end
