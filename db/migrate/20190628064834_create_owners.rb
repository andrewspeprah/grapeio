class CreateOwners < ActiveRecord::Migration[5.2]
  def change
    create_table :owners do |t|
      t.string  :name, default: ""
      t.string  :telephone, default: ""
      t.text    :location, default: ""
      t.string  :role, default: "owner"
      t.timestamps
    end
  end
end
