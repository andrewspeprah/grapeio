class CreateLands < ActiveRecord::Migration[5.2]
  def change
    create_table :lands do |t|
      t.string :size
      t.string :document_type
      t.string :no_of_plots
      t.decimal :price
      t.string :currency
      t.references :owner
      t.timestamps
    end
  end
end
