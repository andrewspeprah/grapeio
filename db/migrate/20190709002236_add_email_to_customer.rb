class AddEmailToCustomer < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :email, :string, :after => "telephone"
    remove_column :customers, :location
  end
end
