class AddCaretakerAndKeeper < ActiveRecord::Migration[5.2]
  def change
    add_column :properties, :caretaker_id, :integer
    add_column :properties, :has_caretaker, :boolean, default: false
  end
end
