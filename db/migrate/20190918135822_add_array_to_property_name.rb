class AddArrayToPropertyName < ActiveRecord::Migration[5.2]
  def change
    rename_column :call_logs, :property_name, :property_names
    change_column :call_logs, :property_names, "varchar[] USING (string_to_array(property_names, ','))"
  end
end
