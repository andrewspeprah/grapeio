class AddAppointmentDate < ActiveRecord::Migration[5.2]
  def change
    add_column :call_logs, :appointment_date, :datetime
  end
end
