class AddStatusToCallLog < ActiveRecord::Migration[5.2]
  def change
    add_column :call_logs, :status, :string, default: "Response pending"
  end
end
