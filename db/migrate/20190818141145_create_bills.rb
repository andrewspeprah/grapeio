class CreateBills < ActiveRecord::Migration[5.2]
  def change
    create_table :bills do |t|
      t.datetime :usage_from
      t.datetime :usage_to
      t.string   :state, default: 'pending'
      t.string   :paid_by
      t.timestamps
    end
  end
end
