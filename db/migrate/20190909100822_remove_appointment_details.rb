class RemoveAppointmentDetails < ActiveRecord::Migration[5.2]
  def change
    remove_column :call_logs, :appointment_date
    remove_column :call_logs, :appointment_time
    remove_column :call_logs, :preference
  end
end
