class AddDataToCallLog < ActiveRecord::Migration[5.2]
  def change
    add_column :call_logs, :preference, :jsonb, null: false, default: '{}'
    add_column :call_logs, :property_id, :integer
    remove_column :call_logs, :property
    remove_column :call_logs, :location
    remove_column :call_logs, :budget
    remove_column :call_logs, :currency
    remove_column :call_logs, :duration
  end
end
