class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :first_name, default: ""
      t.string :last_name, default: ""
      t.string :telephone, default: ""
      t.timestamps
    end
  end
end
