class AddLocationToCustomer < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :location, :text
    add_column :properties, :bedrooms, :integer
    add_column :properties, :bathrooms, :integer
    add_column :properties, :size, :integer
    add_column :properties, :features, :string, array: true, default: []
    add_column :properties, :utilities, :string, array: true, default: []
  end
end
