class AddFieldsToCallLog < ActiveRecord::Migration[5.2]
  def change
    add_column :call_logs, :property_name, :string
    add_column :call_logs, :property_suburb, :string
    add_column :call_logs, :budget_amount, :string
    add_column :call_logs, :budget_currency, :string
    add_column :call_logs, :move_duration, :datetime
  end
end
