class AddDeletedAtToCallLog < ActiveRecord::Migration[5.2]
  def change
    add_column :call_logs, :deleted_at, :datetime
    add_index :call_logs, :deleted_at
  end
end
