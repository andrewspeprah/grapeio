class AddResourceToRole < ActiveRecord::Migration[5.2]
  def change
    add_column :roles, :resource_type, :string
    add_column :roles, :resource_id, :integer
  end
end
