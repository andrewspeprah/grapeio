class CreateCallLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :call_logs do |t|
      t.string :property
      t.string :location
      t.decimal :budget
      t.string  :currency
      t.references :user
      t.references :customer
      t.string :duration
      t.timestamps
    end
  end
end
