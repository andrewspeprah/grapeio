class ChangeCallLogArrayToString < ActiveRecord::Migration[5.2]
  def up
    add_column :call_logs, :property_names_str, :string, default: ""
    add_column :call_logs, :property_locations_str, :string, default: ""

    CallLog.all.each do |call_log|
      call_log.update_attributes(property_locations_str: call_log.property_locations&.join(","),
                                 property_names_str: call_log.property_names&.join(","))
    end

    remove_column :call_logs, :property_locations
    remove_column :call_logs, :property_names

    rename_column :call_logs, :property_names_str, :property_names
    rename_column :call_logs, :property_locations_str, :property_locations
  end

  def down
    change_column :call_logs, :property_names, "varchar[] USING (string_to_array(property_names, ','))"
    change_column :call_logs, :property_locations, "varchar[] USING (string_to_array(property_locations, ','))"
  end
end
