class CreateAppDefaults < ActiveRecord::Migration[5.2]
  def change
    create_table :app_defaults do |t|
      t.string  :field
      t.json    :specs, default: {}
      t.timestamps
    end
  end
end
