class CreatePolicies < ActiveRecord::Migration[5.2]
  def change
    create_table :policies do |t|
      t.references :role
      t.json :specs, default: {}
      t.timestamps
    end
  end
end
