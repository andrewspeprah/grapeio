class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.date :scheduled_date
      t.time :scheduled_time
      t.references :user
      t.references :customer
      t.references :call_log
      t.timestamps
    end
  end
end
