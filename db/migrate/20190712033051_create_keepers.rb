class CreateKeepers < ActiveRecord::Migration[5.2]
  def change
    create_table :keepers do |t|
      t.string :name
      t.string :telephone
      t.string :location
      t.boolean :is_owner, default: true
      t.timestamps
    end
  end
end
