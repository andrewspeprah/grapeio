class AddLandmarktoProperty < ActiveRecord::Migration[5.2]
  def change
    add_column :properties, :landmark, :text
    add_column :properties, :size_unit, :string
    remove_column :properties, :water_source
    add_column :properties, :water_source, :string, array: true, default: []
  end
end
