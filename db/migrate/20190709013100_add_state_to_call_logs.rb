class AddStateToCallLogs < ActiveRecord::Migration[5.2]
  def change
    add_column :call_logs, :state, :string
    add_column :call_logs, :property_code, :string
    add_column :call_logs, :appointment_time, :time
    add_column :customers, :location, :text
    #Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")
  end
end
