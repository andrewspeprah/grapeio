class RemoveDefaultValueProperties < ActiveRecord::Migration[5.2]
  def up
    change_column :properties, :name, :string, default: nil
  end

  def down
    change_column :properties, :name, :string, default: 'SR'
  end
end
