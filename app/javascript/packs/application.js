$(document).ready(function(){
  // Load autocomplete input
  var places = require('places.js');
  const axios = require('axios');

  if(window.location.href.indexOf("properties/") > -1) {
    places({
      appId: 'pl0PLZSDIQHE',
      apiKey: 'f0860cc6a6c4366e701086fafc64f20b',
      container: document.querySelector('#property_owner_attributes_location')
    });

    places({
      appId: 'pl0PLZSDIQHE',
      apiKey: 'f0860cc6a6c4366e701086fafc64f20b',
      container: document.querySelector('#property_caretaker_attributes_location')
    });
  }
  // Gets the geolocation and saves it
  function getLocation() {
    var latlng = $("#property_location_latlng").val();
    if(latlng !== ''){
      latlng = latlng.split(",");
      var lat = latlng[0];
      var lng = latlng[1];
      showPosition({coords: {latitude: lat, longitude: lng}})
    }else{
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      }
    }
  }
  
  function showPosition(position) {
    var coords = position.coords
    var lat = coords.latitude;
    var lng = coords.longitude;

    // Set the lat and lng in the form
    $("#property_location_latlng").val(lat+","+lng);

    // call geocoder to get the address name
    axios.get('/geocodes/location',{params: {lat: lat, lng: lng}})
    .then(function (response) {
      var data = response.data;
      $("#property_location").val(data.address);
      $("#property_location_city").val(data.city);
      $("#property_location_state").val(data.state);
      $("#property_location_suburb").val(data.suburb);
      $("#property_location_road").val(data.road);
      $("#property_location_postcode").val(data.postcode);
    })
    .catch(function (error) {
    })
    .finally(function () {
    });
  }

  // Only get location if the property is being 
  // entered for the first time
  if(window.location.href.indexOf("properties/new") > -1) {
    getLocation();
  }else{
  }

  $(".generate_gps").click(function(){
    getLocation();
  })

  $(".toggler").change(function() {
    if(this.checked) {
      $("div#"+$(this).attr('id')).show();
    }else{
      $("div#"+$(this).attr('id')).hide();
    }
  });
});