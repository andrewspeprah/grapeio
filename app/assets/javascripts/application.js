//= require jquery
//= require jquery-ui
//= require jquery-ui/widgets/datepicker
//= require rails-ujs
//= require jquery.dataTables.js
//= require dataTables.bootstrap4.js
//= require activestorage
//= require vendors/chart.js/Chart.min.js
//= require vendors/datatables.net/jquery.dataTables.js
//= require vendors/datatables.net-bs4/dataTables.bootstrap4.js
//= require off-canvas.js
//= require hoverable-collapse.js
//= require template.js
//= require dashboard.js
//= require data-table.js
//= require wice_grid
//= require gritter
//= require jquery.remotipart
//= require jquery-fileupload/basic
//= require vendors/base/vendor.bundle.base.js
//= require_tree ./channels
