App.messages = App.cable.subscriptions.create('MessagesChannel', {
  received: function(data) {
    this.renderMessage(data);
    return true;
  },

  renderMessage: function(data) {
    jQuery.gritter.add({
      image: '/assets/success.png',
      title: 'Success',
      text: data.message
    }); 
  }
});