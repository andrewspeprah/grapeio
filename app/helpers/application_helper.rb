module ApplicationHelper
  def get_subdomain
    return request.subdomain
  end

  def link_to_add_fields(name, f, association)
      new_object = f.object.send(association).klass.new
      id= new_object.object_id
      fields=f.fields_for(association, new_object, child_index: id) do | builder |
          render(association.to_s.singularize + "_fields", f: builder)
      end
      link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def check_is_admin_rights?
      if current_user.is_admin
        true
      else
        false
      end
  end

  def tag_options(options, escape = true)
    tag_builder.tag_options(options, escape)
  end

  def rejected_key(key)
    ['location_latlng', 'user_id','owner_id', 'state', 'id', 'currency', 'size_unit', 'is_owner', 'location', 'deleted_at'].include? key
  end

  def can?(action, field)
    current_user.privileges_check(action, field)
  end

  def pretty(str_array)
    str_array&.join("<br>")&.html_safe || ""
  end

  def pretty_str(str_array)
    str_array&.split(",")&.join("<br>")&.html_safe || ""
  end
end
