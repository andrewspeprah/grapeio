class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :load_schema, :set_mailer_host
  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :current_account

  def notify_user(message)
    ActionCable.server.broadcast 'messages',
        message: message
  end

  def can?(action, field)
    current_user.privileges_check(action, field)
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:accept_invitation){|u|
      u.permit(:first_name, :last_name, :password, :password_confirmation, :invitation_token, :password_updated_at)
    }

    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :email, :password, :password_confirmation])
  end

  private
    def load_schema
      Apartment::Tenant.switch!('public')
      return unless get_subdomain_acc.present?

      if current_account
        begin
          Apartment::Tenant.switch!(current_account.subdomain_name)
        rescue => exception
          redirect_to root_url(subdomain: false)
        end
      else
        redirect_to root_url(subdomain: false)
      end
    end

    def current_account
      @current_account ||= Account.find_by(subdomain_name: get_subdomain_acc)
    end

    def set_mailer_host
      subdomain = current_account ? "#{current_account.subdomain_name}." : ""
      if Rails.env == "production"
        ActionMailer::Base.default_url_options[:host] = "#{subdomain}<your production name>.com"
      elsif Rails.env == "staging"
        ActionMailer::Base.default_url_options[:host] = "#{subdomain}<your staging name>.com"
      else
        ActionMailer::Base.default_url_options[:host] = "#{subdomain}localhost"
      end
    end

    def get_subdomain_acc
      return request.subdomain
    end

    def after_sign_out_path_for(resource_or_scope)
      new_user_session_path
    end

    def after_invite_path_for(resource)
      #invite_users_path
      root_path
    end
end
