module Users
  class RolesController < UsersController
    before_action :find_role, except: [:index, :new, :create]

    def index
      @roles = Role.all.order('created_at')
    end

    def new
      @role = Role.new
    end

    def edit
    end

    def create
      @role = Role.new(roles_params)
      @role.build_policy
      @role.policy.specs = default_policies
      respond_to do |format|
        if @role.save
          flash[:notice] = 'Role was successfully created.'
          format.html { redirect_to users_roles_path }
        else
          flash[:error] = 'Role was unsuccessfully created.'
          format.html { render action: 'new' }
        end
      end
    end

    def update
      if @role.update_attributes(roles_params)
        flash[:success] = 'Role was successfully updated'
        redirect_to users_roles_path
      else
        flash[:error] = 'Something went wrong'
        render 'edit'
      end
    end

    def destroy
      if @role.destroy
        flash[:success] = 'Role was successfully deleted.'
      else
        flash[:error] = 'Something went wrong'
      end

      redirect_to users_roles_path
    end

    # -- Permissions
    def permissions
      @policy = @role.policy
      @default_policy = default_policies
    end

    def update_permissions
      @policy = @role.policy
      if @policy.update_attributes(policy_params)
        flash[:success] = 'Role permissions was successfully updated'
        redirect_to users_roles_path
      else
        flash[:error] = 'Something went wrong'
        render 'permissions'
      end
    end

    private

    def roles_params
      params.require(:role).permit(:name, policy_attributes: [:name])
    end

    def policy_params
      params.require(:policy).permit(specs: {})
    end

    def find_role
      @role = Role.find(params[:id])
      unless @role.present?
        flash[:error] = 'Role was not found.'
        redirect_to users_roles_path
      end
    end

    def default_policies
      # Set the default privileges
      begin
        file_path = Rails.root.join('db', 'defaults', 'privileges.json')
        json_specs = File.read file_path
        json_specs = JSON.parse(json_specs)
      rescue => exception
        json_specs = {}
      end
      return json_specs
    end
  end
end
