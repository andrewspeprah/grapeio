module Users
  class ProfilesController < UsersController
    before_action :find_user, except: %i[index new create]

    def index
      @users = User.all.order(:created_at)
    end

    def new
      @user = User.new
      @user.roles.build
    end

    # Create a new user
    def create
      @user = User.invite!(user_params, current_user)
      # Todo: Check option autoconfirm
      @user.confirm
      # Todo: check option for auto invitation accept
      User.accept_invitation!(invitation_token: @user.raw_invitation_token, password: user_params[:password])

      redirect_to users_profiles_path
    end

    def edit
    end

    def update
      if @user.update_attributes(user_params)
        flash[:success] = "User details was successfully updated"
        redirect_to users_profiles_path
      else
        flash[:error] = "Something went wrong"
        render 'edit'
      end
    end

    def roles
    end

    def update_roles
    end

    private

    def user_params
      params.require(:user).permit(:first_name,
                                  :last_name,
                                  :email,
                                  :password,
                                  :password_confirmation, role_ids: [])
    end

    def find_user
      @user = User.find params[:id]
      unless @user.present?
        flash[:error] = 'User not found.'
        redirect_to users_profiles_path
        return ;
      end
    end
  end
end
