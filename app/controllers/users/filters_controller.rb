class Users::FiltersController < UsersController
  # params - type
  # 1. property
  def index
    results = 
      case params[:type]
      when 'property'
        Property.new.as_json.keys.sort
      else
        Property.new.as_json.keys.sort
      end

    render json: results, status: :ok
  end

  def options
    unless params[:field].present?
      render json: [], status: :ok
      return ;
    end

    results = 
      case params[:type]
      when 'property'
        Property.select(:"#{params[:field]}").all.pluck(:"#{params[:field]}").uniq.compact.sort
      end

    render json: results, status: :ok
  end
end
