class Users::LandsController < UsersController
  before_action :find_land, only: [:edit, :update, :destroy]

  def index
  end

  def new
  end

  def edit
  end

  def update
    if @land.update_attributes(land_params)
      flash[:success] = "Land details was successfully updated"
      redirect_to users_land_path(@land)
    else
      flash[:error] = "Something went wrong"
      render 'edit'
    end
  end

  def transition
    begin
      if @land.send("#{land_params[:state]}!")
        flash[:success] = "Land is now #{@land.state}."
      else
        flash[:error] = "Error uploading image."
      end
    rescue => exception
      flash[:error] = "Sorry, you are unallowed to '#{land_params[:state]}' property. "
    else
    end

    redirect_to users_land_path(@land)
  end

  private

  def land_params
    params.require(:land).permit(:amount, :currency, :customer_id, :state)
  end

  def find_land
    @land = record_scope.find(params[:id])
    unless @land.present?
      flash[:error] = "Land details not found"
      redirect_to users_lands_path
    end
  end

  def record_scope
    if can?("view_all", "lands")
      Land.where(deleted_at: nil).order(created_at: :desc)
    else
      current_user.lands.order(created_at: :desc)
    end
  end
end
