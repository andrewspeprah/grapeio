module Users
  class GeocodesController < UsersController
    def location
      results = Geocoder.search([params[:lat], params[:lng]])
      result = results.first
      address = result.data['address']
      render json: {address: result.address,
                    city: address['city'],
                    state: address['state'],
                    suburb: address['suburb'],
                    road: address['road'],
                    postcode: address['postcode']}, status: :ok
    end
  end
end
