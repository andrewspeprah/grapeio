class Users::BillingsController < UsersController
  def index
    respond_to do |format|
      format.html {
        @billing_grid = initialize_grid(Bill,
                                        name: 'bill',
                                        enable_export_to_csv: true)
        export_grid_if_requested('bill' => 'bill_grid')
      }
    end
  end
end
