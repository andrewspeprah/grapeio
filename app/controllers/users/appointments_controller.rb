class Users::AppointmentsController < UsersController
  before_action :find_call_log, except: %i[index]
  before_action :find_appointment, except: %i[index new create]

  def index
    respond_to do |format|
      format.html {
        @appointments_grid = initialize_grid(Appointment,
                                          name: 'appointment',
                                          include: [:call_log, :user, :customer],
                                          enable_export_to_csv: true,
                                          order:  'appointments.created_at',
                                          order_direction: 'desc')
        export_grid_if_requested('appointment' => 'appointment_grid')
      }
    end
  end

  def new
    @appointment = @call_log.appointments.new
    @appointment.user_id = current_user.id
    @appointment.customer_id = @call_log.customer_id
  end

  def create
    @appointment = @call_log.appointments.new(appointment_params)
    @appointment.user_id = current_user.id
    @appointment.customer_id = @call_log.customer_id

    if @appointment.save
      flash[:success] = "Appointment successfully created"
      redirect_to users_call_log_path(@call_log)
    else
      flash[:error] = "Something went wrong"
      render 'new'
    end
  end

  def edit
  end

  def update
    if @appointment.update_attributes(appointment_params)
      flash[:success] = "Appointment was successfully updated"
      redirect_to users_call_log_path(@call_log)
    else
      flash[:error] = "Something went wrong"
      render 'edit'
    end
  end

  def destroy
    if @appointment.destroy
      flash[:success] = 'Appointment was successfully deleted.'
    else
      flash[:error] = 'Something went wrong'
    end

    redirect_to users_call_log_path(@call_log)
  end

  private

  def appointment_params
    params.require(:appointment).permit(:scheduled_date, :scheduled_time, :state)
  end

  def find_call_log
    unless params[:cid].present?
      flash[:alert] = "Call log not found"
      redirect_back fallback_location: users_call_logs_path
      return ;
    end

    @call_log = CallLog.find params[:cid]
    unless @call_log.present?
      flash[:alert] = "Call log not found"
      redirect_back fallback_location: users_call_logs_path
      return ;
    end
  end
  
  def find_appointment
    @appointment = @call_log.appointments.find params[:id]
    unless @appointment.present?
      flash[:alert] = "Appointment not found"
      redirect_back fallback_location: users_call_logs_path
    end
  end
end
