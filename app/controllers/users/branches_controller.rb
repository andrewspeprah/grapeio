module Users
  class BranchesController < UsersController
    before_action :find_branch, only: [:edit, :update, :destroy]

    def index
      @branches = Branch.all
    end

    def new
      @branch = Branch.new
    end

    def create
      @branch = Branch.new(branch_params)
      if @branch.save
        flash[:success] = 'Branch successfully created.'
        redirect_to users_branches_path
      else
        flash[:error] = 'Something went wrong'
        render 'new'
      end
    end

    def edit
    end

    def update
      if @branch.update_attributes(branch_params)
        flash[:success] = 'Branch was successfully updated'
        redirect_to users_branches_path
      else
        flash[:error] = 'Something went wrong'
        render 'edit'
      end
    end

    def destroy
      if @object.destroy
        flash[:success] = 'Branch was successfully deleted.'
      else
        flash[:error] = 'Something went wrong'
      end

      redirect_to users_branches_path
    end


    private

    def branch_params
      params.require(:branch).permit(:name, :code, :telephone, :location)
    end

    def find_branch
      @branch = Branch.find(params[:id])
      unless @branch.present?
        flash[:error] = 'Branch not found.'
        redirect_to users_branches_path
      end
    end
  end
end
