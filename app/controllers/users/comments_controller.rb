class Users::CommentsController < UsersController
  before_action :find_call_log
  before_action :find_comment, except: [:new, :create]

  def new
    @comment = @call_log.comments.new
    @comment.user_id = current_user.id
  end

  def create
    @comment = @call_log.comments.new(comment_params)
    @comment.user = current_user
    if @comment.save
      @call_log.update_attribute(:status, @comment.state)
      flash[:success] = "Comment successfully created"
      redirect_to users_call_log_path(@call_log)
    else
      flash[:error] = "Something went wrong"
      render 'new'
    end
  end

  def edit
  end

  def update
    if @comment.update_attributes(comment_params)
      @call_log.update_attribute(:status, @call_log.comments.last&.state || @call_log.status)
      flash[:success] = "Comment was successfully updated"
      redirect_to users_call_log_path(@call_log)
    else
      flash[:error] = "Something went wrong"
      render 'edit'
    end
  end

  def destroy
    if @comment.destroy
      @call_log.update_attribute(:status, @call_log.comments.last&.state || "Response pending")
      flash[:success] = 'Comment was successfully deleted.'
    else
      flash[:error] = 'Something went wrong'
    end

    redirect_to users_call_log_path(@call_log)
  end


  private
  def comment_params
    params.require(:comment).permit(:details, :state)
  end

  def find_call_log
    unless params[:cid].present?
      flash[:alert] = "Call log not found"
      redirect_back fallback_location: users_call_logs_path
      return ;
    end

    @call_log = CallLog.find params[:cid]
    unless @call_log.present?
      flash[:alert] = "Call log not found"
      redirect_back fallback_location: users_call_logs_path
      return ;
    end
  end

  def find_comment
    @comment = @call_log.comments.find params[:id]
    unless @comment.present?
      flash[:alert] = "comment not found"
      redirect_back fallback_location: users_call_logs_path
    end
  end
end
