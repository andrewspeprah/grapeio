class Users::PhotosController < UsersController
  before_action :find_property

  def index
    @images = @property.images.map(&:service_url)
    render json: {images: @images}, status: :ok
  end

  private
  def find_property
    @property = current_user.properties.find_by_id(params[:property_id])
    unless @property.present?
      render json: {error: 'Property not found'}, status: :failed
      return ;
    end
  end
end
