module Users
  class PropertiesController < UsersController
    before_action :find_property, except: [:index, :new, :create]
    before_action :set_current_user

    def index
      respond_to do |format|
        format.html {
          @filterrific = initialize_filterrific(
            record_scope.page(params[:page]),
            params[:filterrific],
            persistence_id: false
          ) || return

          @properties_grid = initialize_grid(@filterrific.find.page(params[:page]),
                                            name: 'property',
                                            order:  'properties.created_at',
                                            order_direction: 'desc')
        }

        format.json {
          build_query
          render json: properties, status: :ok
        }

        format.csv do
          export = ExportProperties.new
          export.collection = properties
          send_data export.to_csv
        end
      end
    end

    def show
      if current_user.can?("transition", "properties")
        if @property.pending?
          @property.review!
        end
      end
    end

    def new
      @property = current_user.properties.new
      @property.build_owner
      @property.build_caretaker
    end

    def create
      respond_to do |format|
        save_property
        format.html {
          if @property.save
            flash[:success] = "Property successfully created"
            redirect_to users_properties_path
          else
            flash[:error] = "Something went wrong"
            render 'new'
          end
        }

        # Creae a property and save it
        format.js {
          @property.save
        }
      end
    end

    def edit
    end

    def update
      if @property.update_attributes(property_params)
        flash[:success] = "Property was successfully updated"
        redirect_to users_property_path(@property)
      else
        flash[:error] = "Something went wrong"
        render 'edit'
      end
    end

    def upload_pic
      if @property.update_attributes(property_params)
        flash[:success] = "Property image uploaded."
      else
        flash[:error] = "Error uploading image."
      end

      redirect_to users_property_path(@property)
    end

    def remove_pic
      image = @property.images.find(params[:iid])
      if image.destroy
        flash[:success] = "Image removed."
      else
        flash[:error] = "Error removing image."
      end

      redirect_to users_property_path(@property)
    end

    def transition
      begin
        if @property.send("#{property_params[:state]}!")
          flash[:success] = "Property is now #{@property.state}."
        else
          flash[:error] = "Error uploading image."
        end
      rescue => exception
        flash[:error] = "Sorry, you are unallowed to '#{property_params[:state]}' property. "
      else
      end

      redirect_to users_property_path(@property)
    end


    def destroy
      if @property.destroy
        flash[:success] = 'Property was successfully removed.'
      else
        flash[:error] = 'Something went wrong'
      end
      redirect_back fallback_location: users_properties_path
    end

    # Payments
    # new payment form
    def new_rent
      @payment = @property.payments.new(transaction_type: 'rent')
    end

    def new_sale
      @payment = @property.payments.new(transaction_type: 'buy')
    end

    # save the payment
    def payment
      @payment = @property.payments.new(payment_params)
      if @payment.save
        if @payment.transaction_type == 'sale'
          @property.sell!
        else
          @property.rent_out!
        end

        flash[:success] = "Payment successfully processed."
        redirect_to users_property_path(@property)
      else
        flash[:error] = "There was an error processing payment. Please try again."
        render "new_#{payment_params[:transaction_type]}"
      end
    end

    # download property images
    def download_pics
      process_and_create_zip_file
    end

    def process_and_create_zip_file
      # Tmp folder to store the download files from S3
      # Notice we prfix the folder with a unique number (current_user.id)
      tmp_user_folder = "tmp/archive_#{current_user.id}"
      if File.exist? tmp_user_folder
        FileUtils.rm_rf([tmp_user_folder, "#{tmp_user_folder}.zip"])
      end

      # Determin the length of the folder
      directory_length_same_as_documents = Dir["#{tmp_user_folder}/*"].length == @property.images.count
      # Create a tmp folder if not exists
      FileUtils.mkdir_p(tmp_user_folder) unless Dir.exists?(tmp_user_folder)
      # Download and save documents to our tmp folder
      @property.images.where(id: params[:images]).each do |document|
        filename = document.blob.filename.to_s
        # User should be able to download files if not yet removed from tmp folder
        # if the folder is already there, we'd get an error
        create_tmp_folder_and_store_documents(document, tmp_user_folder, filename) unless directory_length_same_as_documents
        #---------- Convert to .zip --------------------------------------- #
        create_zip_from_tmp_folder(tmp_user_folder, filename) unless directory_length_same_as_documents
      end
      # Sends the *.zip file to be download to the client's browser
      send_file(Rails.root.join("#{tmp_user_folder}.zip"), :type => 'application/zip', :filename => "photos_#{@property.code}.zip", :disposition => 'attachment')

      # as zip file wont be able to downloads if uncommented
      # FileUtils.rm_rf([tmp_user_folder, "#{tmp_user_folder}.zip"])
    end
    
    def create_tmp_folder_and_store_documents(document, tmp_user_folder, filename)
      File.open(File.join(tmp_user_folder, filename), 'wb') do |file|
       document.download { |chunk| file.write(chunk) }
      end
    end
    
    def create_zip_from_tmp_folder(tmp_user_folder, filename)
      require 'zip'

      Zip::File.open("#{tmp_user_folder}.zip", Zip::File::CREATE) do |zf|
        zf.add(filename, "#{tmp_user_folder}/#{filename}")
      end
    end
    
    
    private

    def property_params
      params.require(:property).permit(:offer_type, :category, :build_type, :name, :price, :currency,
                                      :tenure, :location, :location_city, :location_state, :landmark,
                                      :location_latlng, :location_suburb, :location_road, :location_postcode,
                                      :water_source, :meter,:no_of_tenant,
                                      :other_info, :user_id, :owner_id, :state, :bedrooms, :bathrooms,
                                      :size, :size_unit, :features, :has_caretaker,
                                      owner_attributes: [:name, :telephone, :location],
                                      caretaker_attributes: [:name, :telephone, :location],
                                      :utilities => [], :images => [], :features => [],
                                      :water_source => [])
    end

    def payment_params
      params.require(:payment).permit(:amount, :currency, :customer_id, :seller_id, :date_sold, :transaction_type, :rent_from, :rent_to, :other_details)
    end

    def find_property
      @property = record_scope.find(params[:id])
      unless @property.present?
        flash[:error] = "Property not found"
        redirect_to users_properties_path
      end
    end

    def build_query
      query = {}
      params[:filters]&.each do |filter|
        filter = JSON.parse(filter)
        if is_int_param(filter['name'])
          query[:"#{filter['name']}"] = (filter['selectedOptions'].first)..(filter['selectedOptions'].last)
        else
          query[:"#{filter['name']}"] = filter['selectedOptions']
        end
      end

      @properties = Property.where(query).order("created_at desc")
    end

    def is_int_param(field)
      ['size','price','bedrooms','bathrooms'].include? field
    end

    def generate_code(name)
      index = Property.with_deleted.where(name: name).count + 1
      index = "%.2i" %index
      "#{name}-#{current_user.initials}-#{index}"
    end

    def save_property
      @property = current_user.properties.new(property_params)
      @property.code = generate_code(property_params[:name])
      @property.location_suburb = @property.location_suburb.strip.titleize

      if params[:commit] == 'Temporarily Save Property'
        @property.state = 'temp'
      end
    end

    def set_current_user
      User.current_user = current_user
    end

    def record_scope
      if can?("view_all", "properties")
        Property.where(deleted_at: nil).order(created_at: :desc)
      else
        current_user.properties.order(created_at: :desc)
      end
    end
    # EOF private
  end
end

# ["H6", "CH", "H4", "B4", "B3", "B2", "SR", "H2", "H3", "B5+", "H1", "B1", "1", "H5", "B7", "B6", ""].each do |property_name|
#   Property.with_deleted.where(name: property_name).order("created_at ASC")&.each_with_index do |property, index|
#     new_code = property.code.split("-")
#     *new_code, i = new_code
#     new_code = new_code << "%.2i" %index
#     pp property.update_attribute(:code, new_code.join("-"))
#     pp property.id
#   end
# end
