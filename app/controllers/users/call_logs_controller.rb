module Users
  class CallLogsController < UsersController
    before_action :find_call_log, except: [:index, :new, :create]

    def index
      respond_to do |format|
        format.html {
          @filterrific = initialize_filterrific(
            record_scope.page(params[:page]),
            params[:filterrific],
            persistence_id: false
          ) || return

          @call_logs_grid = initialize_grid(@filterrific.find.page(params[:page]),
                                            name: 'call_log',
                                            include:  [:customer, :user, :comments],
                                            order:  'call_logs.created_at',
                                            order_direction: 'desc')
          export_grid_if_requested('call_log' => 'call_log_grid')
        }
  
        format.json {
        }
      end
    end

    def new
      @call_log = current_user.call_logs.new
      @call_log.build_customer
    end

    def create
      @log = current_user.call_logs.new(log_params)
      if @log.save
        flash[:success] = "Call Log successfully created."
        redirect_to users_call_log_path(@log)
      else
        flash[:error] = "Something went wrong while creating call log."
        render 'new'
      end
    end

    def edit
    end

    def update
      if @call_log.update_attributes(log_params)
        flash[:success] = "Call Log was successfully updated"
        redirect_to users_call_log_path(@call_log)
      else
        flash[:error] = "Something went wrong"
        render 'edit'
      end
    end

    def destroy
      if @call_log.destroy
        flash[:success] = 'Call Log was successfully deleted.'
      else
        flash[:error] = 'Something went wrong'
      end

      redirect_to users_call_logs_path
    end

    def transition
      begin
        if @call_log.update_attribute(:state, log_params[:state])
          flash[:success] = "Call Log is now #{@call_log.state.humanize}."
        else
          flash[:error] = "Error changing status"
        end
      rescue => exception
        flash[:error] = "Sorry, you are unallowed to '#{log_params[:state]}' property. "
      end

      redirect_to users_call_log_path(@call_log)
    end

    private

    def log_params
      params.require(:call_log).permit(:budget_currency, :budget_amount, :status,
                                  :property_locations, :property_names,
                                  customer_attributes: [:first_name, :last_name, :telephone])
    end

    def record_scope
      if can?("view_all", "call_logs")
        CallLog.includes(:user).where(deleted_at: nil).order(created_at: :desc)
      else
        current_user.call_logs.includes(:user).where(deleted_at: nil).order(created_at: :desc)
      end
    end

    def find_call_log
      @call_log = record_scope.find(params[:id])
      unless @call_log.present?
        flash[:error] = "Call Log not found"
        redirect_to users_call_logs_path
      end
    end
  end
end
