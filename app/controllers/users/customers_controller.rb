class Users::CustomersController < UsersController
  def index
    respond_to do |format|
      format.html { 
        @q = Customer.ransack(params[:q])
        @customers = @q.result(distinct: true).page params[:page]
       }

      format.json {
      }
    end
  end
end
