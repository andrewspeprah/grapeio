class HomeController < ApplicationController
  layout "home"

  def index
  end

  def welcome
  end

  def billing
  end
end
