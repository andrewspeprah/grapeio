
class AccountsController < ApplicationController
  
  def index
  end
  
  def forgot
  end

  def new
    @account = Account.new
    @account.build_owner
  end

  def create
    @account = Account.new(accounts_params)
    @account.subdomain_name = get_subdomain(@account.name)
    if @account.valid?
      Apartment::Tenant.create(@account.subdomain_name)
      Apartment::Tenant.switch!(@account.subdomain_name)
      if @account.save
        redirect_to new_user_session_url(subdomain: @account.subdomain_name)
      else
        render action: 'new'
      end
    else
      render action: 'new'
    end
  end

  def find_team
    team_name = get_subdomain(params[:team_name])
    if team_name.present? && !team_name.nil? && !team_name.blank?
      Apartment::Tenant.switch!('public')
      if search_team(team_name)
        begin
          Apartment::Tenant.switch!(team_name)
          redirect_to new_user_session_url(subdomain: team_name)
        rescue => exception
          flash[:alert] = "Team Not Found."
          redirect_to accounts_path
        end
      else
        flash[:alert] = "Team Not Found."
        redirect_to accounts_path
      end
    else
      flash[:alert] = "Please Enter the Team Name."
      redirect_to accounts_path
    end
  end

  def remember
  end

  protected
  def search_team(team_name)
    @account ||= Account.find_by(subdomain_name: team_name)
  end

  private
  def accounts_params
    params.require(:account).permit(:name, owner_attributes: [:first_name, :last_name, :is_admin, :email, :password, :password_confirmation])
  end 

  def get_subdomain(str)
    str.strip.downcase.gsub(" ", "-")
  end
end
