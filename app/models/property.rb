class Property < ApplicationRecord
  include DefaultScope
  include PropertyAasm

  has_paper_trail
  acts_as_paranoid

  OFFER_TYPE = ["Sale", "Rent", "Short stay", "Long lease"]

  CATEGORY = ["CAT.A", "CAT.B", "CAT.C"]

  BUILDING_TYPE = ['Apartment', 'Self Compound', 'Condominium',
                   'Compound house', 'Office', 'Hostel',
                   'Commercial space', 'Town house',
                   'Shop'].freeze

  PROPERTY_NAME = %w[SR CH B2 B3 B4 B5+ H2 H3 H4 H+ OF LA].freeze

  CURRENCIES = ['GHS', 'USD'].freeze

  TENURE = ['Daily', 'Weekly', 'Monthly', 'Yearly', 'Lease hold', 'Free hold'].freeze

  TENURE_SALE = ['Freehold', 'Lease hold'].freeze

  METER = ['Self-meter', 'Joint'].freeze

  FEATURES = ['Fully Furnished', 'Semi-furnished', 'Unfurnished'].freeze

  UTILITIES = ['Roof terrace', 'Gym', 'Swimming pool',
               'Security Post', 'Security service',
               'CCTV', 'Air Conditioner', 'Microwave',
               'Cooker', 'Fan', 'Refrigerator',
               'Washing Machine', 'Garage / car parking limit',
               'Garden', 'Boys Quarters'].freeze

  STATES = ['pending', 'review', 'approve',
            'publish', 'unpublish', 'available',
            'archive', 'rent out', 'sold out']

  SIZE_UNIT = ['m²', 'sq ft'].freeze

  WATER_SOURCE = ['Pipe', 'Borehole', 'Storage tank'].freeze

  # Relationships
  belongs_to :owner, class_name: 'Keeper'
  belongs_to :caretaker, class_name: 'Keeper', optional: true
  validates  :owner, presence: true
  validates_presence_of :owner, :offer_type, :build_type, :currency, :price, :tenure, :size, :size_unit

  scope :not_deleted, -> { where(deleted_at: nil) }
  validates :code, presence: true, uniqueness: { constraint: -> { not_deleted } }

  has_many :payments, dependent: :destroy

  accepts_nested_attributes_for :owner, allow_destroy: true
  accepts_nested_attributes_for :caretaker, allow_destroy: true

  before_save :sanitaze_location

  ## == relationships
  belongs_to :user
  has_many_attached :images

  # -- Filters -- 
  # ["id", "offer_type", "category", "code", "build_type", "name", "price", "currency",
  # "tenure", "location", "location_latlng", "meter", "no_of_tenant", "other_info",
  # "state", "bedrooms", "bathrooms", "size", "features", "utilities",
  # "landmark", "size_unit", "water_source", "location_city", "location_state",
  # "location_postcode", "location_road", "location_suburb"] 

  scope :search_query, ->(query) {
    where("LOWER(code) ILIKE :query OR LOWER(name) ILIKE :query OR LOWER(location_suburb) ILIKE :query", query: "%#{query&.downcase&.strip}%")
  }

  scope :price_gte, ->(query) {
    where("price >= :query", query: query)
  }

  scope :price_lte, ->(query) {
    where("price <= :query", query: query)
  }

  filter_keys = []
  self.attribute_names.each do |s|
    next if %w[id owner_id user_id caretaker_id state].include? s

    scope :"with_#{s}", ->(query) {
      where("LOWER(#{s}) ILIKE :query", query: "%#{query&.downcase&.strip}%")
    }
    filter_keys << :"with_#{s}"
  end

  filterrific(
    available_filters: [
      :search_query,
      :with_state,
      :with_created_at_gte,
      :with_created_at_lte,
      :with_id,
      :with_name,
      :price_gte,
      :price_lte,
      filter_keys
    ].flatten
  )

  # -- Filters -- 

  def save_approver
    update_attributes(approved_by: User.current_user.name, approved_at: Time.now)
  end

  # Scope - Available | Unavailable
  # available - pending | approved | published
  # unavailable  sold | archived
  # rejected rejected - Hard delete | Archive

  ## == Callbacks

  def full_price
    "#{currency} #{price}"
  end

  # Images
  def self.sizes
    {
      thumbnail: { resize: "720x480" },
      hero1:     { resize: "1920x1080" }
    }
  end

  def sized(size)
    self.image_file.variant(Property.sizes[size]).processed
  end

  private
  def sanitaze_location
    self.location_suburb = self.location_suburb&.strip&.titleize
  end
end
