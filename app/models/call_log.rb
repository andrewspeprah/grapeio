class CallLog < ApplicationRecord
  # include AASM
  include DefaultScope

  # aasm :column => 'state' do
  #   state :response_pending, initial: true
  #   state :appointment_pending
  #   state :on_tour
  #   state :tour_complete
  #   state :deal_closed
  #   state :sorted_elsewhere
  #   state :lost_interest
  #   state :unrealistic_request
  #   state :unrealistic_budget
  #   state :facility_unavailable
  # end

  # STATES = ['response_pending',
  #           'appointment_pending',
  #           'on_tour',
  #           'tour_complete',
  #           'deal_closed',
  #           'sorted_elsewhere',
  #           'lost_interest',
  #           'unrealistic_request',
  #           'unrealistic_budget',
  #           'facility_unavailable']

  scope :search_query, ->(query) {
    where("LOWER(property_names) ILIKE :query OR LOWER(property_locations) ILIKE :query", query: "%#{query&.downcase&.strip}%")
  }

  scope :with_customer, ->(query) {
    where("EXISTS (SELECT 1 from customers WHERE call_logs.customer_id = customers.id AND (LOWER(customers.first_name) ILIKE :query OR LOWER(customers.last_name) ILIKE :query OR customers.telephone ILIKE :query))", query: "%#{query&.downcase&.strip}%")
  }

  filter_keys = []
  self.attribute_names.each do |s|
    next if %w[id owner_id user_id caretaker_id].include? s

    scope :"with_#{s}", ->(query) {
      where("LOWER(#{s}) ILIKE :query", query: "%#{query&.downcase&.strip}%")
    }
    filter_keys << :"with_#{s}"
  end

  filterrific(
    available_filters: [
      :search_query,
      :with_state,
      :with_created_at_gte,
      :with_created_at_lte,
      :with_id,
      :with_name,
      :with_customer,
      filter_keys
    ].flatten
  )

  # Relationships
  belongs_to :customer
  belongs_to :user, optional: true
  has_many :comments, dependent: :destroy
  has_many :appointments, dependent: :destroy

  # Nested Attributes
  accepts_nested_attributes_for :customer, allow_destroy: true
  accepts_nested_attributes_for :comments, allow_destroy: true
end
