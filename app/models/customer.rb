class Customer < ApplicationRecord
  paginates_per 10
  has_many :call_logs
  has_many :payments
  has_many :appointments

  def name
    "#{first_name} #{last_name}"
  end
end
