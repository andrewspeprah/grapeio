class User < ApplicationRecord
  rolify
  resourcify

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :validatable,
         :trackable, :confirmable, :timeoutable

  belongs_to :branch, optional: true
  has_many :properties
  has_many :call_logs
  has_many :comments
  has_many :payments
  has_one  :account
  has_and_belongs_to_many :roles, :join_table => :users_roles

  # Nested attributes
  accepts_nested_attributes_for :roles

  def name
    "#{first_name} #{last_name}"
  end

  def initials
    "#{first_name[0]}#{last_name[0]}"
  end

  def self.current_user
    Thread.current[:user]
  end

  def self.current_user=(user)
    Thread.current[:user] = user
  end

  def privileges_check(action, field)
    current_privileges[field].try(:[], action) || false
  end

  def can?(action, field)
    privileges_check(action, field)
  end

  def current_privileges
    @privileges ||= build_privileges
  end

  private

  def build_privileges
    temp_hash = {}
    roles.map(&:policy).pluck(:specs).each do |spec| 
      spec.each do |key, values|
        unless temp_hash[key].present?
          temp_hash[key] = {}
        end
        temp_hash[key] = values
      end
    end
    return temp_hash
  end
end
