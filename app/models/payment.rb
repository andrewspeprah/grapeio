# Payments made by customers
class Payment < ApplicationRecord
  belongs_to :property

  belongs_to :seller, class_name: 'User'
  belongs_to :customer
end
