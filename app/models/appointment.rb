class Appointment < ApplicationRecord
  # include AASM
  include DefaultScope

  belongs_to :call_log
  belongs_to :user
  belongs_to :customer

  STATES = ['Date Fixed', 'Date Pending', 'On Tour', 'Tour complete', 'Deal closed'].freeze

  scope :search_query, ->(query) {
    where("LOWER(property_names) ILIKE :query OR LOWER(property_locations) ILIKE :query", query: "%#{query&.downcase&.strip}%")
  }

  scope :with_customer, ->(query) {
    where("EXISTS (SELECT 1 from customers WHERE appointments.customer_id = customers.id AND (LOWER(customers.first_name) ILIKE :query OR LOWER(customers.last_name) ILIKE :query OR customers.telephone ILIKE :query))", query: "%#{query&.downcase&.strip}%")
  }

  filter_keys = []
  self.attribute_names.each do |s|
    next if %w[id user_id].include? s

    scope :"with_#{s}", ->(query) {
      where("LOWER(#{s}) ILIKE :query", query: "%#{query&.downcase&.strip}%")
    }
    filter_keys << :"with_#{s}"
  end


  filterrific(
    available_filters: [
      :search_query,
      :with_state,
      :with_created_at_gte,
      :with_created_at_lte,
      :with_customer,
      filter_keys
    ].flatten
  )

end
