class Role < ApplicationRecord
  has_and_belongs_to_many :users, :join_table => :users_roles
  has_one :policy, dependent: :destroy
  validates :name, presence: :true, uniqueness: true
  accepts_nested_attributes_for :policy, allow_destroy: true

  belongs_to :resource,
            :polymorphic => true,
            :optional => true


  validates :resource_type,
            :inclusion => { :in => Rolify.resource_types },
            :allow_nil => true

  scopify
end
