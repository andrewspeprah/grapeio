module DefaultScope
  include ActiveSupport::Concern

  def self.included(klass)
    klass.instance_eval do
      scope :with_created_at_gte, ->(query) {
        where("created_at >= :query", query: query)
      }

      scope :with_created_at_lte, ->(query) {
        where("created_at <= :query", query: query)
      }

      scope :with_id, ->(query) {
        where("id = :query", query: query)
      }

      scope :with_state, ->(query) {
        where("state = lower(:query)", query: query)
      }
    end
  end
end