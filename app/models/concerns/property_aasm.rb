module PropertyAasm
  include ActiveSupport::Concern
  
  def self.included(klass)
    klass.instance_eval do
      include AASM

      ## == State machine
      aasm :column => 'state' do
        state :pending, initial: true
        state :reviewed
        state :approved
        state :published
        state :unpublished
        state :archived
        state :rejected
        state :selling
        state :sold
        state :renting
        state :rented_out

        event :review do
          transitions from: [:pending, :approved], to: :reviewed
        end

        event :approve, after: :save_approver do
          transitions from: :reviewed, to: :approved
        end

        event :publish do
          transitions from: [:unpublished, :approved], to: :published
        end

        event :unpublish do
          transitions from: :published, to: :unpublished
        end

        event :"sold out" do
          transitions from: [:approved, :published], to: :sold
        end

        event :"rent out" do
          transitions from: [:approved, :published], to: :rented_out
        end

        event :available do
          transitions from: [:rented_out, :published], to: :approved
        end
      end
    end
  end
end