class Comment < ApplicationRecord
  belongs_to :call_log
  belongs_to :user

  STATES = ["Response pending", "Unreachable", "Not answered", "Client feed back pending",
            "Officer's feedback pending", "Unrealistic request",
            "Sorted elsewhere", "Request unavailable", "Lost interest", "Responded To"]
end
