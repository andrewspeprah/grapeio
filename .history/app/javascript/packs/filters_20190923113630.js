import Vue from 'vue/dist/vue.js';
import Buefy from 'buefy';
import Filters from 'views/components/filter';
import MultiSelect from 'views/components/multiselect';
import Accordion from 'accordion-js';
import vSelect from 'vue-select';
import mSelect from 'vue-multiselect'
import VueSlider from 'vue-slider-component'
import {ServerTable, ClientTable, Event} from 'vue-tables-2';

$(document).ready(function(){
  // Set components
  Vue.component('v-select', vSelect)
  Vue.component('m-select', mSelect)
  Vue.component('v-slider', VueSlider)
  Vue.use(ClientTable);

  new Vue({
    el: '#data-vue',
    components: {
      'filters': Filters,
      'multiselect': MultiSelect
    }
  });
})