# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

# config valid only for current version of Capistrano
set :application, 'grapeio'
set :repo_url, 'git@bitbucket.org:andrewspeprah/grapeio.git'
set :user, 'greywolf'

set :puma_threads,    [4, 16]
set :puma_workers,    0

# Seting ruby version
set :rvm_type, :user
set :rvm_ruby_version, '2.5.3'


# Don't change these unless you know what you're doing
set :pty,             false
set :use_sudo,        false
set :stage,           :production
# set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord


## Defaults:
# set :scm,           :git
set :branch,        :master
# set :format,        :pretty
# set :log_level,     :debug
set :keep_releases, 8

## Linked Files & Directories (Default None):
set :linked_files, %w{config/database.yml}
set :linked_dirs,  %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/packs .bundle node_modules}

before "deploy:assets:precompile", "deploy:yarn_install"

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  desc "Run rake yarn install"
  task :yarn_install do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && yarn install --silent")
      end
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma

# nginx configuration
set :nginx_sites_available_path, "/etc/nginx/sites-available"
set :nginx_sites_enabled_path, "/etc/nginx/sites-enabled"


# Default settings
set :foreman_use_sudo, true # Set to :rbenv for rbenv sudo, :rvm for rvmsudo or true for normal sudo
set :foreman_roles, :all
set :foreman_template, 'systemd'
set :foreman_export_path, ->{ '/etc/init.d' }
set :foreman_options, ->{ {
  app: "/home/#{fetch(:user)}/apps/#{fetch(:application)}/current",
  log: "/home/#{fetch(:user)}/apps/#{fetch(:application)}/shared/log"
} }
