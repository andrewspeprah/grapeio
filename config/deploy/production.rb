# ------------------------------------
server "172.105.26.230",
  user: "greywolf",
  roles: %w{web app db},
  ssh_options: {
    keys: %w(/Users/andrewspeprah/.ssh/id_rsa),
    forward_agent: false,
    auth_methods: %w(publickey password)
  }
