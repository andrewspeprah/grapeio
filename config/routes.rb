class SubdomainPresent
  def self.matches?(request)
    subdomains = %w{ www }
    request.subdomain.present? && !subdomains.include?(request.subdomain)
  end
end

class SubdomainBlank
  def self.matches?(request)
    request.subdomain.blank?
  end
end

Rails.application.routes.draw do
  mount Maily::Engine, at: '/maily'
  get 'accounts/new'

  # Action cable
  mount ActionCable.server => '/cable'

  constraints(SubdomainPresent) do
    root 'users#index', as: :subdomain_root

    # users - routes
    devise_for :users, path: ''
    resources :users, path: '', only: :index
    namespace :users, path: '' do
      resources :appointments
      resources :branches
      resources :comments
      resources :profiles do
        member do
          get :roles
          patch :update_roles
        end
      end
      resources :roles do
        member do
          get :permissions
          patch :update_permissions
        end
      end
      resources :settings
      resources :help, only: :index
      resources :customers
      resources :call_logs do
        member do
          post  :create_comment
          patch :transition
        end
      end
      resources :lands
      resources :properties do
        member do
          patch :upload_pic
          get   :remove_pic
          patch :transition
          get   :new_rent
          get   :new_sale
          post  :payment
          patch :download_pics
        end
      end
      resources :geocodes do
        collection do
          get :location
        end
      end
      resources :filters do
        collection do
          get :options
        end
      end
    end
  end  

  constraints(SubdomainBlank) do 
    root 'home#index'
    resources :accounts, only: [:index, :new, :create] do
      collection do
        get :forgot
        post :remember
        post :find_team
      end
    end

    # admins - routes
    resources :admins, only: :index
    devise_for :admins
  end

  # IP blacklisting for admin website
end
