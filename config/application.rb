require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :developmesnt, or :production.
Bundler.require(*Rails.groups)

module Grapeio
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.generators do |g|
      g.test_framework :rspec
    end

    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    config.assets.enabled = true
    config.action_controller.permit_all_parameters = true

    if Rails.env.production?
      Raven.configure do |config|
        config.dsn = ENV['SENTRY_URL']
      end
    end
  end
end
