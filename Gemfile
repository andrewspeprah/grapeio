source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

gem 'bootsnap', '>= 1.1.0', require: false
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'rails', '~> 5.2.1'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  gem 'pry'
  gem "capistrano", "~> 3.7", require: false
  gem 'capistrano-figaro-yml', '~> 1.0.2'
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
  gem 'capistrano-sidekiq'
  # gem "capistrano-resque",  require: false
  gem 'capistrano-foreman', require: false
  gem 'capistrano-webpacker-precompile', require: false
  # For hiding passwords
  gem 'highline'
  gem 'fabrication'
  gem 'solargraph'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'chromedriver-helper'
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
  gem 'selenium-webdriver'
  gem 'simplecov'
end

# DB Loader
gem 'faker'

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'devise'
gem 'apartment'
gem 'devise_invitable'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Environment variables
gem 'figaro'
gem 'foreman'
gem 'slim'
gem 'webpacker'
gem 'noty-rails'

# Background works
gem 'sidekiq'
gem 'sidekiq-status'

# Track model's changes
gem 'paper_trail'

# Event pattern
gem 'aasm'

# Pagination
gem 'kaminari', "~> 0.16"

# Font icons and jquery
gem 'font-awesome-rails'
gem 'jquery-ui-rails'
gem 'jquery-rails'


# Form building
gem 'simple_form'

# Roles and permissions
gem 'rolify'
gem 'cancancan'

# Search
gem 'ransack'

# Location search
gem 'geocoder'

# Notifications
gem 'gritter'

# Background
gem 'redis'
gem 'redis-namespace'

# Async image upload
gem 'dragonfly'
gem 'dragonfly-s3_data_store'
group :production do
  gem 'rack-cache', :require => 'rack/cache'
end
gem 'bootstrap-sass'
gem 'remotipart', '~> 1.2'
gem 'jquery-fileupload-rails', github: 'Springest/jquery-fileupload-rails'

# Soft delete
gem 'paranoia'

# zip files download
gem 'rubyzip'
gem 'aws-sdk-s3'

# rails console
gem 'rack-console'

# Sentry
gem "sentry-raven"
gem "newrelic_rpm"

# Mailer
gem 'maily'

# Grid
gem "wice_grid", '3.6.0.pre4'
gem 'font-awesome-sass',  '~> 4.3'
gem 'bootstrap-datepicker-rails'
gem 'lp_csv_exportable'

# Analytics
gem "hirb"

# Image processing
gem "mini_magick"

# Fixing route issues
gem "route_downcaser"

# Filter and search
gem 'filterrific'
gem 'nested_form'
gem 'addressable'